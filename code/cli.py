# -*- coding: utf-8 -*-
"""Command line interface to run the module transcript_sampler.

Command line interface (CLI) using the argparse module to allow a user to pass
and run the module transcript_sampler outside of a Python interpreter.

  Typical usage example:

  $ python cli.py input_file sample_size
"""

import argparse

import os
import sys

import transcript_sampler as tr


if __name__ == '__main__':
    # Create the parser
    parser = argparse.ArgumentParser(prog='transcript_sampler',
                                     description='Generates a sample \
                                     of transcripts')

    # Add the arguments
    parser.add_argument('InFile',
                        metavar='input_file',
                        type=str,
                        help='name of input file including the path')

    parser.add_argument('TotalNumber',
                        metavar='sample size',
                        type=int,
                        help='number of transcripts to sample')

    # Execute the parse_args() method
    args = parser.parse_args()

    # Check if input file exists.
    input_file = args.InFile

    if input_file is None or not os.path.exists(input_file):
        print('The file specified does not exist')
        sys.exit()

    # Check if input number is of type Int.
    input_number = args.TotalNumber

    if not isinstance(input_number, int):
        print('Provided Number is not of type Int')
        sys.exit()

    # Run module transcript_sampler with provided command line arguments.
    ts = tr.TranscriptSampler()

    geneID_nrCopies_dict = ts.read_avg_expression(input_file)

    sample = ts.sample_transcripts(geneID_nrCopies_dict, input_number)

    ts.write_sample('sample_dict.txt', sample)

    # print("__end of script 'cli.py'__")

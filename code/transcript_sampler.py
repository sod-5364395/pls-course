# -*- coding: utf-8 -*-
"""Module contains one class generating a stochastic sample of transcripts.

The class TranscriptSampler() generates a stochastic sample of transcribed
copies of genes.

  Typical usage example:

  ts = TranscriptSampler()
  dict_avg = ts.read_avg_expressions(input_file_with_path)
  dict_sample = ts.sample_transcripts(dict_avg, sample_size)
  ts.write_sample(output_file_name, dict_sample)
"""

import argparse
import numpy as np
from collections import Counter


class TranscriptSampler:
    """Generates a stochastic sample of transcribed copies of a gene.

    Based on the relative abundance of the number of transcripts of genes, a
    stochastic sample of transcripts is generated.

    Attributes:
        None
    """

    def read_avg_expression(self, fileName):
        """Reads average expression values of genes from file.

        The function gets as an input a text file containing gene names and
        average expression values. Gene names and expression values are put
        in a dictionary where the gene name is the key.

        Args:
            fileName (str): name of input file including path

        Returns:
            dict: dictionary of converted input file

        """
        geneID_nrCopies = {}
        with open(fileName, 'r') as file:
            for line in file:
                line = line.strip('\n')
                splitted = line.split(' ')
                geneID_nrCopies[splitted[0]] = int(splitted[1])
        return geneID_nrCopies

    def sample_transcripts(self, avgs, number):
        """Creates a sample based on average gene expressions.

        The function gets as an input the dictionary created by the function
        read_avg_expression(fileName). Based on the relative abundance of the
        number of transcripts in the dictionary, the function samples a
        given number of transcripts and returns the sample as a dictionary.

        Args:
            avgs (dict): dictionary from read_avg_expression(fileName)
            number (int): total number of sampled transcripts

        Returns:
            dict: dictionary of sampled transcripts

        """
        key_list = np.array(list(avgs.keys()))
        val_list = np.array(list(avgs.values()), dtype=float)
        sum_val = sum(val_list)
        val_list = val_list / sum_val
        sample_arr = np.random.choice(key_list,
                                      number,
                                      replace=True,
                                      p=val_list)
        sample = Counter(sample_arr)
        sample_dict = dict(sample)
        return sample_dict

    def write_sample(self, file, sample):
        """Writes a dictionary into a file.

        The function gets as an input the dictionary created by the function
        sample_transcripts(avgs, number) and the name of the output file
        including the path. The dictionary is written into the output file.

        Args:
            file (str): name of output file including path
            sample (dict): dictionary from sample_transcripts(avgs, number)

        Returns:
            None

        """
        with open(file, 'w') as file:
            for (k, v) in sample.items():
                file.write(k + ' ' + str(v) + '\n')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Input Arguments
    parser.add_argument('--fileName',
                        default='geneID_nrCopies.txt',
                        required=False)

    parser.add_argument('--totalNumber',
                        default=10000,
                        required=False)

    args = parser.parse_args()

    ts = TranscriptSampler()

    geneID_nrCopies_dict = ts.read_avg_expression(args.fileName)

    sample = ts.sample_transcripts(geneID_nrCopies_dict, args.totalNumber)

    ts.write_sample('sample_dict.txt', sample)

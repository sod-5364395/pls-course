from setuptools import setup, find_packages

setup(
    name='TranscriptSamplerPLS',
    url='https://gitlab.com/sod-5364395/pls-course.git',
    author='David Sommer',
    author_email='sommedav@student.ethz.ch',
    description='Generates a stochastic sample of transcribed copies of a gene, given the average expression level of genes.',
    license='MIT',
    version='1.0.0',
    packages=find_packages(),  # this will autodetect Python packages from the directory tree, e.g., in `code/`
    install_requires=[],  # add here packages that are required for your package to run, including version or range of versions
    entry_points = {
        'console_scripts': ['my-executable=TranscriptSamplerPLS.cli:main'],
    },
)